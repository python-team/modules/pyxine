# This file was created automatically by SWIG.
import pxlibc
class PxDisplayPtr :
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def __del__(self):
        if self.thisown == 1 :
            pxlibc.delete_PxDisplay(self.this)
    def has_windows(self):
        val = pxlibc.PxDisplay_has_windows(self.this)
        return val
    def __repr__(self):
        return "<C PxDisplay instance>"
class PxDisplay(PxDisplayPtr):
    def __init__(self,arg0) :
        self.this = pxlibc.new_PxDisplay(arg0)
        self.thisown = 1




class PxWindowPtr :
    def __init__(self,this):
        self.this = this
        self.thisown = 0
    def __del__(self):
        if self.thisown == 1 :
            pxlibc.delete_PxWindow(self.this)
    def get_window_geometry(self):
        val = pxlibc.PxWindow_get_window_geometry(self.this)
        return val
    def get_xine_x11_visual(self):
        val = pxlibc.PxWindow_get_xine_x11_visual(self.this)
        return val
    def set_xine_stream(self,arg0):
        val = pxlibc.PxWindow_set_xine_stream(self.this,arg0)
        return val
    def get_verbosity(self):
        val = pxlibc.PxWindow_get_verbosity(self.this)
        return val
    def set_verbosity(self,arg0):
        val = pxlibc.PxWindow_set_verbosity(self.this,arg0)
        return val
    def get_pixel_aspect(self):
        val = pxlibc.PxWindow_get_pixel_aspect(self.this)
        return val
    def invalidate_cache(self):
        val = pxlibc.PxWindow_invalidate_cache(self.this)
        return val
    def __repr__(self):
        return "<C PxWindow instance>"
class PxWindow(PxWindowPtr):
    def __init__(self,arg0,arg1,arg2,arg3) :
        self.this = pxlibc.new_PxWindow(arg0.this,arg1,arg2,arg3)
        self.thisown = 1






#-------------- FUNCTION WRAPPERS ------------------



#-------------- VARIABLE WRAPPERS ------------------

